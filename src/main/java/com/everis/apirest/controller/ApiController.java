package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.CustomerRedResource;
import com.everis.apirest.controller.resource.CustomerResource;
import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;
import com.everis.apirest.service.CustomerService;

//import com.everis.apirest.controller.resource.ProductoResource;
//import com.everis.apirest.service.ProductoService;

@RestController
public class ApiController {
	
	@Autowired
    CustomerRepository customerRepository;
   
    @RequestMapping(method = RequestMethod.POST, value = "/customers")
    public CustomerResource addCustomer(@RequestBody CustomerRedResource customerRedResource, ModelMap mp) {
        Customer customerNuevo = new Customer();
        StringTokenizer lstApellidos = new StringTokenizer(customerRedResource.getApellidoCompleto()); 
        Long idNuevo = 1 + customerService.obtenerUltimoId();
        customerNuevo.setId(idNuevo);
        customerNuevo.setName(customerRedResource.getNombre());       
        customerNuevo.setLastName(lstApellidos.nextToken());
        customerNuevo.setLastName2(lstApellidos.nextToken());       
        customerRepository.save(customerNuevo);
        
        mp.put("Customer", customerNuevo);
        customerNuevo = customerService.obtenerCustomerPorID(customerNuevo.getId());
        CustomerResource customerResource =  new CustomerResource();
        customerResource.setId(customerNuevo.getId());
        customerResource.setNombre(customerNuevo.getName());
        customerResource.setApellidoCompleto(customerNuevo.getLastName()+" "+customerNuevo.getLastName2());       
        return customerResource;
    }
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/customers")
	public List<CustomerResource> obtenerCustomers(){
		
		List<CustomerResource> listado = new ArrayList<>();
		customerService.obtenerCustomers().forEach(customer ->{
			CustomerResource customerResource =  new CustomerResource();
			customerResource.setId(customer.getId());
			customerResource.setNombre(customer.getName());
			customerResource.setApellidoCompleto(customer.getLastName()+ " " + customer.getLastName2());
			listado.add(customerResource);
		});
		
		return listado;
	}
}
