package com.everis.apirest.controller.resource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NameResource {
	
	private String name;
	private String description;
	private int version;
		

}
