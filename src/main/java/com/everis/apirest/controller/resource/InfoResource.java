package com.everis.apirest.controller.resource;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoResource {
	
	public String nombre;
	public String usuario;
	public Date fechaSistema;

}
