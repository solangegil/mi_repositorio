package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class CustomerRedResource {
	private String nombre;
	private String apellidoCompleto;
}
