package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Iterable<Customer> obtenerCustomers() {
		// TODO Auto-generated method stub
		return customerRepository.findAll();
	}

	@Override
	public Customer insertar(Customer customer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long obtenerUltimoId() {
		return customerRepository.count();
	}

	@Override
	public Customer obtenerCustomerPorID(Long idn) {
		java.util.Optional<Customer> customer_opt = customerRepository.findById(idn);
		Customer customer = customer_opt.get();
		return customer;
	}
	
}

